import 'package:flutter/material.dart';

import '../model/persona.dart';

class PersonaWidget extends StatelessWidget {
  final Persona persona;
  final Function onTap;

  const PersonaWidget({Key? key, required this.persona, required this.onTap})
      : super(key: key);

  // Read comments in order 1-2-3 please.
  @override
  Widget build(BuildContext context) {
    // 3 - Finally, as we want our widget to be able to respond to some gestures, we wrap
    // everything using a GestureDetector widget. In this cas we only use the double tap
    // gesture, but we could add more properties to detect more gestures using this same
    // GestureDetector, no need to add new widgets.
    return GestureDetector(
      onTap: () {
        //gesto al hacer doble click en la persona
        onTap(persona);
      },
      // 2 - As we want some free space around texts, we wrap the Column widget with this
      // Padding widget. Its sole purpose is to add some padding around Column.
      child: Container(
        padding: const EdgeInsets.all(18.0),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.blueAccent)
        ),
        // 1 - Column is actually "main" widget in this build() method. We want to show
        // two strings and use a Column for that. Nothing new here, nothing fancy.
        child: Column(
          
          children: [  
            Icon(Icons.directions_transit),
            Text(persona.nombre.toString()),
            Text(persona.profesion.toString()),
            Text(persona.puntuacion.toString()),
          ],
        ),
      ),
    );
  }
}
