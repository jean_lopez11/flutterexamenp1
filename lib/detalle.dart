import 'package:cartoons_flutter/model/persona.dart';
import 'package:flutter/material.dart';

class Detalle extends StatelessWidget {
  // const Detalle({ Key? key }) : super(key: key);
  Detalle(this.persona);
  
  final Persona persona;

// class _DetalleState extends State<Detalle> {
  @override
  Widget build(BuildContext context) {
    // final args = ModalRoute.of(context)!.settings.arguments as Persona;
    return Scaffold(
      appBar: AppBar(title: Text("Detalle")),
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(18.0),
        
          child: Column(
            children: [
              Icon(Icons.directions_transit),
              Text(persona.nombre),
              Text(persona.profesion),
              Text(persona.puntuacion),
              ElevatedButton(onPressed: ()=> Navigator.pop(context) , child: Text("Done"),)

            ],
          ),
        ),
      )
    );
  }
}