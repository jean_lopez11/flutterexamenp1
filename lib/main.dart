import 'package:cartoons_flutter/detalle.dart';
import 'package:cartoons_flutter/widgets/persona_widget.dart';
import 'package:flutter/material.dart';
// import 'package:cartoons_flutter/model/characters_data.dart';
// import 'package:cartoons_flutter/widgets/character_widget.dart';

import 'model/persona.dart';

void main() {
  runApp(const MyApp());
}
  final personas = [
    Persona(nombre: "Jose Nuñez", profesion: 'Estudiante del 9no Nivel de T.I', puntuacion: "4.0"),
    Persona(nombre: "Juan Salvador", profesion: 'Estudiante del 7no Nivel de T.I', puntuacion: "4.0"),
    Persona(nombre: "Pedro Gomez", profesion: 'Profesor de T.I', puntuacion: "4.0"),
    Persona(nombre: "Leonel Messi", profesion: 'Jugador de Futbol PSG', puntuacion: "4.0"),
    Persona(nombre: "Cristiano Ronaldo", profesion: 'Jugador de Futbol Manchester', puntuacion: "4.0"),
    Persona(nombre: "Piero Hincapie", profesion: 'Jugador de Futbol Leverkusen', puntuacion: "4.0"),
    Persona(nombre: "Richard Carapaz", profesion: 'Ciclista INEO Grenadiers', puntuacion: "4.0"),
  ];

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('EXAMEN JEAN LOPEZ 8VO T.I'),
      ),
      body: ListView(
        children: personas
        .map((persona) => PersonaWidget(persona: persona, onTap: 
          // doSomething
          (doSomething){
          Navigator.push(context, MaterialPageRoute(builder: (context){
            return Detalle(persona);
          }));
        }
        ))
        .toList(),
        
      ),
    );
  }
}
void ScreenArguments (String nombre,String profesion, String puntuacion) {
  // final String nombre;
  // final String profesion;
  // final String puntuacion;
  ScreenArguments(nombre,profesion,puntuacion);
  
}

  void doSomething(Persona persona) {
    // ignore: avoid_print
    print(persona.nombre);
    // Navigator.push(context, MaterialPageRoute(builder: (_)=> Detalle()));
  }
 